Si la lettre hamza ou le soukoun ne sont pas suivis d'une lettre Madd, en raison de leur “nature”, la lettre Madd n'est prolongée que d'un seul alif (ou d'une seule intonation). On appelle cela Madd-i Asli (le Madd Naturel ou Original). Un autre nom pour cela est Madd-i Tabee’ee.
L'application du Madd-i Tabee’ee est obligatoire, c'est pourquoi nous l'appelons Madd-i Waajib.
Veuillez prêter attention aux lettres en rouge qui indiquent un prolongement dans ces exemples.
Commençons la pratique 😊 :