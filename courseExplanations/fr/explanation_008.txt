• Le Shadda est formé en rapprochant deux lettres identiques : la première avec un jazm, la deuxième avec une voyelle. Il est récité avec emphase.
• Le Shadda est écrit comme un "w" ou un "m" inversé au-dessus de la lettre.
•  Par exemple : اِنْ نَ, dans ce mot, deux nuns se suivent. Le premier est avec un jazm, le second avec une voyelle courte, donc ils sont écrits comme un seul nun avec un Shadda au-dessus (اِنَّ) et récités avec emphase. Translittération : le premier est ‘in-na’, le second est ‘innnna’.
• Une lettre avec un Shadda ne commence pas un mot.
• Le Kasra peut être écrit sous la lettre ou sous le Shadda.
• Lorsque le nun et le mim ont un Shadda, ils produisent un son de ghunna plus prononcé que les autres lettres.
Commençons 😊 :
