Mari kita mulai melakukan sesi tanya jawab untuk menginternalisasi beberapa istilah dan definisi mendasar tentang aturan yang terkait dengan tanwin dan Nun Sukun (ْن dan ً ٍ ٌ) dalam pembacaan Al -Qur'an sebelum menjadi canggih.

T: Apa itu tanwin?

J: tanwin berarti dua fatha (NASB), dua Kasra (jar), dan dua dhamma (RAF) dalam Al -Qur'an (ً ٍ ٌ).

T: Apa itu Nun sukun?

J: Nun sukun berarti Nun dengan Sukun (atau Jazm) di atasnya (ْن).

T: Apa masalahnya dengan tanwin dan Nun sukun?

J: Dalam Al -Qur'an, di mana pun Anda membaca, jika Anda melihat tanwin dan Nun sukun, akan selalu ada aturan pembacaan yang terkait dengan pengucapan huruf Nun.

T: Berapa banyak dan apa aturan itu?

J: Ada empat aturan yang terkait dengan tanwin dan Nun sukun. Yaitu, (1) Idzhar, (2) idgham, (3) Iqlab, dan (4) Ikhfa.

T: Apa kriteria yang akan saya gunakan untuk menentukan aturan-aturan itu dalam Al-Qur'an?

J: Untuk aturannya, Anda harus melihat huruf setelah Nun sukun atau tanwin untuk menemukan aturan mana yang akan diterapkan.

T: Harap uraikan aturan dan huruf-huruf yang akan membantu kita menentukan aturan mana yang akan diterapkan

J: Pasti! Izinkan saya memberi Anda formula untuk setiap aturan:

1. Idzhar: (ا, ح, خ, ع, غ, ه)+(نْ atau ـًـٍـٌ)

2.a.Idghaam dengan ghunnah: (ن, و, ي, م)+(نْ atau ـًـٍـٌ)

2.b.Idghaam tanpa ghunnah: (ل, ر)+(نْ atau ـًـٍـٌ)

3. Iqlab: (ب)+(نْ atau ـًـٍـٌ)

4. Ikhfa': (λ, ث, ج, د, ذ, ز, س, ش, ص, ض, ط, ظ, ف, ق, ك)+(نْ atau ـًـٍـٌ)

T: Anda mengatakan hanya ada empat aturan tetapi Anda menunjukkan 5!Jadi, apa itu Ghunnah?

J: Biasanya, ghunnah adalah setiap kali shadda ّ muncul pada Nun ن atau mim م, seorang pembaca harus menggetarkan suara melalui hidung. pada Idghaam dengan aturan Ghunnah, kita melakukan hal yang sama dengan 4 huruf di atas terkait dengan aturan ini karena kita harus melewatkan Nun dan menghubungkan huruf sebelum Nun dengan huruf setelah Nun dengan tasydid; Kita melakukan hal yang sama untuk Idghaam tanpa Ghunnah tetapi kita tidak membuat suara melalui hidung yang merupakan perbedaan antara Idghaam dengan Ghunnah dan Idghaam tanpa Ghunnah. Dalam contoh, Anda akan memahaminya dengan lebih baik.

T: Bagaimana saya bisa menghafal semua huruf ini? Bukankah terlalu sulit?

J: Tidak, bukan begitu! Biarkan saya memberi Anda petunjuk. Huruf  Idzhar adalah huruf tenggorokan.huruf IQlaab hanya ب, hanya ada 6 huruf idhgaam, dan sisanya akan menjadi huruf ikhfaa

T: Apakah ada petunjuk lain yang ingin Anda berikan kepada saya untuk memudahkan pekerjaan saya?

J: Ya, ada!Jika Anda melihat tanwin dan Sâkin Nun (ْن dan ً ٍ ٌ) di mana saja dalam Al -Qur'an, harus ada 4 aturan yang terkait dengan pengucapan Nun hari.Anda akan (1) mengucapkan (atau menunjukkan bunyi) Nun ( Idzhar), (2) Anda akan menyembunyikan Nun (ikhfaa), (3) Anda akan melewatkan Nun dan menghubungkan huruf sebelum dan sesudah Nun (idghaam), dan akhirnya (4) Anda akan mengubah suara Nun menjadi suara mim (iqlab). Itu dia!

T: Apakah ada pengecualian di tanwin dan Nun sukun?

J: Ya, ada kata-kata dalam Al -Qur'an yang memenuhi persyaratan tanwin dan Sâkin Nun (ْن dan ً ٍ ٍ), tetapi kita tidak melakukan apa -apa, baca saja.

T: Mari kita mulai berlatih!

J: OK. Ayo lakukan!Sekarang Anda dapat beralih ke pelajaran berikutnya.Semoga beruntung!