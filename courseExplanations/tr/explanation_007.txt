﻿• Tenvin, çiftli hareke (iki üstün, iki esre ya da iki ötre) anlamına gelir.
• Tenvin, bir kelimenin sonunda cezm olan bir nûn gibidir, ancak farklı şekilde yazılmıştır.
• Tenvin, harekelere bir "n" sesi eklenerek yapılır.
• İki üstünde kalın harfler (an) ince harfler (en) şeklinde okunur. (mesela بً bann)
• İki esrede ses (in) (mesela تٍ tin)
• İki ötrede ses (un) (mesela سٌ sunn)
• Şimdi aşağıdaki sözcüklerle alıştırma yapalım. Tahmin etmeye çalışın ve doğru yapıp yapmadığınızı kontrol etmek için kelimeye tıklayın.
Başlayalım 😊: