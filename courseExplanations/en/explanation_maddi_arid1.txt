If a temporary sukoon appears on a word that comes after one of the Madd letters, then this is known as Madd-i Âarid
Ârid means temporary, so we can translate it as Temporary Madd.
Madd-i Âarid becomes Madd-i Tabee’ee if the reciter doesn’t pause on the letter and prolongs to only one level of alif (or musical note).
Depending on the last haraka of a word, Madd-i Âarid can be changed in its level of elongation.
Please see the examples and definitions below and pay attention to the red-colored letters of the words. 
Now, let’s start practicing 😊:
1) Madd-i Âarid when the last letter of a word is fatha: As you see in these examples, there is a madd letter and normally sukoon is not there, but when the reciter stop at the end of a verse or pause to take a breath, a temporary sukoon comes there. Therefore, elongation will be determined based on the last haraka. In this case, it is fatha. When you recite, you should elongate 1 to 4 alif or musical notes.