Si hamza o sukoon no aparecen después de una letra Madd, debido a su “naturaleza”, la letra Madd se prolonga sólo una cuenta de alif (o una nota musical). Esto se conoce como Madd-i Asli (El Madd Natural o El Original). Otro nombre es Madd-i Tabee'ee.
La aplicación de Madd-i Tabee'ee es Waajib, por eso lo llamamos Madd-i Waajib.
Preste atención a las letras de color rojo que se prolongan en estos ejemplos.
Empecemos a practicar 😊: