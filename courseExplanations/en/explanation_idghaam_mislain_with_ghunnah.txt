﻿It occurs when a Mim (مَ مِ مُ) with haraka comes after a Mim with sukoon (مْ )or a Noon with harakah (نَ نِ نُ ) comes after a Noon with sukoon ( نْ ). In such a case, the letters Mim (م) and Noon (ن) are pronounced with shaddah and with ghunnah (nasal sound). This is called “idghaam mislayn with ghunnah”.

(نَ نِ نُ+نْ)	(مَ مِ مُ+مْ)

Let’s start practicing 😎.