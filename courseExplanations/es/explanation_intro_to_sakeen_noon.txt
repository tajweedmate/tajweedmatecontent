Comencemos con una sesión de preguntas y respuestas para internalizar algunos de los términos y definiciones fundamentales sobre las reglas relacionadas con Tanween y Sâkin Noon (ْن y ً ٍ ٌ) en la recitación del Corán antes de volvernos sofisticados.

P: ¿Qué es Tanween?

R: Tanween significa dos Fatha (Nasb), dos Kasra (Jar) y dos Dhamma (Raf) en el Corán (ً ٍ ٌ).

P: ¿Qué es Saakin Noon?

R: Mediodía Saakin significa Mediodía con Sukoon (o Jazm) (ْن).

P: ¿Cuál es el trato entre Tanween y Noon Saakin?

Respuesta: En el Corán, dondequiera que leas, si ves a Tanween y Noon Saakin, SIEMPRE habrá una regla de recitación relacionada con la pronunciación de la letra del mediodía.

P: ¿Cuántas y cuáles son estas reglas?

R: Hay cuatro reglas relacionadas con Tanween y Noon Saakin. Es decir, (1) Izhaar, (2) Idhghaam, (3) Iqlaab y (4) Ikhfa.

P: ¿Cuál es el criterio que utilizaré para señalar esas reglas en el Corán?

R: Para las reglas, debes mirar la carta después del mediodía Saakin o Tanween para determinar qué regla aplicar.

P: Por favor, explique más detalladamente estas reglas y cartas que nos ayudarán a determinar qué regla aplicar.

R: ¡Definitivamente! Déjame darte una fórmula para cada regla:

1.Izhaar : (ا,ح,خ,ع,غ,ه)+(نْ o ـًـٍـٌ)

2.a. Idghaam con Ghunnah : (ن,و,ي,م)+(نْ o ـًـٍـٌ)

2.b. Idghaam sin Ghunnah: (ل,ر)+(نْ o ـًـٍـٌ)

3. Iqlaab : (ب)+(نْ o ـًـٍـٌ)

4. ikhfaa : (ت,ث,ج,د,ذ,ز,س,ش,ص,ض,ط,ظ,ف,ق,ك)+(نْ o ـًـٍـٌ)

P: ¡Dijiste que solo hay cuatro reglas pero mostraste 5! Entonces, ¿qué es Ghunnah?

R: Normalmente, ghunna es que cada vez que aparece un shadda ّ al mediodía ن o Meem م, el recitador debe hacer vibrar el sonido a través de la nariz. En idghaam con la regla ghunna, hacemos lo mismo con las 4 letras anteriores relacionadas con esta regla, ya que debemos omitir el mediodía y conectar la letra antes del mediodía y la letra después del mediodía con shadda; Hacemos lo mismo con idghaam sin ghunna pero no emitimos el sonido por la nariz, que es la diferencia entre idghaam con ghunna e idghaam sin ghunna. Con ejemplos lo entenderás mejor.

P: ¿Cómo puedo memorizar todas estas letras? ¿No es demasiado difícil?

R: ¡No, no lo es! Déjame darte una pista. Las letras de Izhaar son letras de garganta. La letra iqlaab es solo ب, solo hay 6 letras idhgaam y el resto serán letras ikhfaa.

P: ¿Hay alguna otra pista que quieras darme para facilitar mi trabajo?

R: ¡Sí, lo hay! Si ves Tanween y Sâkin Noon (ْن y ً ٍ ٌ) en cualquier lugar del Corán, debe haber 4 reglas relacionadas con la pronunciación del mediodía. (1) pronunciarás (o mostrarás) el mediodía (Izhaar), (2) ocultarás el mediodía (ikhfaa), (3) te saltarás el mediodía y conectarás letras antes y después del mediodía (idghaam), y finalmente ( 4) transformarás el sonido del mediodía en sonido meem (Iqlaab). ¡Eso es todo!

P: ¿Hay alguna excepción en Tanween y Noon Saakin?

Respuesta: Sí, hay palabras en el Corán que cumplen con los requisitos de Tanween y Sâkin Noon (ْن y ً ٍ ٌ), pero no hacemos nada, simplemente leemos tal como están.

P: ¡Entonces empecemos a practicar!

R: Está bien. ¡Vamos a hacerlo! Ahora puedes pasar a la siguiente lección. ¡Buena suerte!