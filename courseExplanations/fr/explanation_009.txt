• En arabe, le préfixe ال vient avant les noms pour les rendre définis.
• Le ل dans ce préfixe forme parfois un shadda avec la lettre qui le suit et est parfois récité ouvertement avec un jazm.
• Si l'une de ces lettres ء هـ ب غ ح ج ك و خ ف ع ق ي م vient après ل alors elle est récitée ouvertement avec un jazm. Ces lettres sont appelées lettres qamari, ou lunaires. Le nom vient du mot lune اَلْقَمَرُ.
• Si une autre lettre que celles énumérées ci-dessus vient après lam, lam forme un shadda avec la lettre qui la suit. Ces lettres sont appelées shamsi, ou solaires. Le nom vient du mot soleil اَلشَّمْسُ pour lequel cette règle s'applique.
