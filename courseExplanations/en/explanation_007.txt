• Tanween means doubling of the short vowel signs into Fathatayn (Two Fathas), Dammatayn (Two Dammas), and Kasratayn (Two Kasras).
• Tanween is like a nun with jazm at the end of a word but is written differenly.
• Tanween is made by adding a ‘n’ sound to the short vowels.
• In Fathatayn, non-emphatic letters are recited (EN) and emphatic letters are recited (AN). (i.e. بً bann)
• In Kasratayn, the sound is (İN) (i.e. تٍ tin)
• In Dammatayn, the sound is (UN) (i.e. سٌ sunn)
• Now let’s practice the words below. Try to guess and simply click on the word to check if you did correctly.
Let’s start 😊: