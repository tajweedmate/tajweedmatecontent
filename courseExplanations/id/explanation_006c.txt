• MADD berarti memperpanjang.
• Ketika huruf dengan vokal pendek diikuti oleh huruf yang tidak memiliki vokal pendek, vokal huruf sebelumnya memanjang selama ukuran satu alif (sekitar satu detik).
• Huruf ini tanpa vokal disebut huruf Madd (ا و ي), yang berarti vokal panjang.
• Jika vokal panjang (و) muncul setelah suatu huruf, suaranya diperpanjang hingga dua kali lipat panjang vokal pendek dan dibacakan dengan suara ‘uu’.
• Huruf sebelum vokal panjang (و) selalu memiliki dhommah.
• Dalam beberapa contoh, huruf Alif setelah vokal panjang (و) menandakan pluralitas dan tidak mempengaruhi pembacaan dengan cara apa pun.
Mari kita mulai 😊: