• Madd significa prolongar.
• Cuando una letra con vocal corta va seguida de una letra que no tiene vocal corta, la vocal de la letra anterior se alarga hasta la medida de un Alif (aproximadamente un segundo).
• Esta letra sin vocal se llama letra madd (ا و ي), que significa vocal larga.
• La vocal corta de la letra anterior a Alif (ا) es siempre una fatha.
Empecemos 😊: